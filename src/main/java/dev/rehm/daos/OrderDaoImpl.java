package dev.rehm.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import dev.rehm.models.BakeryItem;
import dev.rehm.models.Bread;
import dev.rehm.models.Coffee;
import dev.rehm.models.Muffin;
import dev.rehm.models.Order;
import dev.rehm.models.OrderStatus;
import dev.rehm.models.Size;
import dev.rehm.util.ConnectionUtil;

public class OrderDaoImpl implements OrderDao{

	@Override
	public List<Order> getAllOrders() {
		List<Order> orders = new ArrayList<>();
		try(Connection connection = ConnectionUtil.getConnection();
				Statement orderPrepared = connection.createStatement();){

			ResultSet rs = orderPrepared.executeQuery("select * from {oj bakery_order left outer join "
					+ "bakery_order_item on (bakery_order.id=bakery_order_item.order_id) left outer join "
					+ "bakery_item on (bakery_order_item.item_id=bakery_item.id) }");

			while(rs.next()) { // for each record
				int orderId = rs.getInt("ID");
				boolean existsOrder = false;
				for(Order order: orders) {
					if(orderId==order.getOrderNumber()) {
						existsOrder = true;

						// if the order already exists, we add the item to the existing order's items 
						BakeryItem item = itemFactory(rs);
						order.getItems().add(item);

						break; // stop checking the order numbers if we've already found one that matches
					} 
				}
				if(!existsOrder) {
					// create new order if it did not already exist
					Order order = orderFactory(rs);
					BakeryItem item = itemFactory(rs);
					if(item!=null) {
						order.getItems().add(item);
					}

					orders.add(order);	
				}
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return orders;
	}

	/*
	 * This method ended up needing a lot of exception handling that we were handling in the same way - this 
	 * is a great use case for throwing exception from the method and using an outer method to handle all of these 
	 * exceptions in the same way. See "createNewOrder" for the wrapped method, which is public and can be used 
	 * in other areas on the program
	 */
	private Order createNewOrderHelper(Order order) throws SQLException {
		String orderSQL = "INSERT INTO BAKERY_ORDER (CUSTOMER, PHONE, HAS_PAID, ORDER_STATUS) VALUES (?,?,?,?)";
		String idSQL = "SELECT MAX(ID) FROM BAKERY_ORDER";
		String itemSQL = "INSERT INTO BAKERY_ORDER_ITEM (ORDER_ID, ITEM_ID) VALUES (?, ?) ";

		Connection connection = null;
		try{
			connection = ConnectionUtil.getConnection();
			try(PreparedStatement orderPStatement = connection.prepareStatement(orderSQL);
					Statement statement = connection.createStatement();
					PreparedStatement itemPStatement = connection.prepareStatement(itemSQL);){

				connection.setAutoCommit(false); //this begins the transaction
				orderPStatement.setString(1, order.getCustomerName());
				orderPStatement.setString(2, order.getPhone());
				orderPStatement.setString(3, order.isHasPrepaid()?"Y":"N"); // ternary operation, abbreviates if/else
				orderPStatement.setString(4, order.getStatus().name());
				orderPStatement.execute();

				ResultSet rs = statement.executeQuery(idSQL);
				if(rs.next()) {
					int orderId = rs.getInt(1);	
					for(BakeryItem item: order.getItems()) {
						itemPStatement.setInt(1, orderId);
						itemPStatement.setInt(2, item.getId());
						itemPStatement.executeUpdate();
					}
				}
				connection.commit();
			}
		} catch (SQLException e) {
			if(connection!=null) {
				connection.rollback();
			}
			e.printStackTrace();
		} 
		finally {
			if(connection!=null) {
				connection.close();
			}
		}
		return order;
	}

	/* Our previous createNewOrder method that does not use transactions
	@Override
	public Order createNewOrder(Order order) {
		String orderSQL = "INSERT INTO BAKERY_ORDER (CUSTOMER, PHONE, HAS_PAID, ORDER_STATUS) VALUES (?,?,?,?)";
		String idSQL = "SELECT MAX(ID) FROM BAKERY_ORDER";
		String itemSQL = "INSERT INTO BAKERY_ORDER_ITEM (ORDER_ID, ITEM_ID) VALUES (?, ?) ";

		try(PreparedStatement orderPStatement = connection.prepareStatement(orderSQL);
					Statement statement = connection.createStatement();
					PreparedStatement itemPStatement = connection.prepareStatement(itemSQL);){

				orderPStatement.setString(2, order.getPhone());
				orderPStatement.setString(3, order.isHasPrepaid()?"Y":"N"); // ternary operation, abbreviates if/else
				orderPStatement.setString(4, order.getStatus().name());
				orderPStatement.execute();

				ResultSet rs = statement.executeQuery(idSQL);
				if(rs.next()) {
					int orderId = rs.getInt(1);	
					for(BakeryItem item: order.getItems()) {
						itemPStatement.setInt(1, orderId);
						itemPStatement.setInt(2, item.getId());
						itemPStatement.executeUpdate();
					}
				}
		} catch (SQLException e) {
			e.printStackTrace();
		} 
		return order;
	}
	 */

	public BakeryItem itemFactory(ResultSet resultSet) throws SQLException {
		String itemType = resultSet.getString("ITEM_TYPE");
		if(itemType != null) { //this can be a null value if the order has no items (because this is a left join)
			switch(itemType) {
			case "MUFFIN":
				Muffin muffin = new Muffin();
				muffin.setId(resultSet.getInt("ID"));
				muffin.setPrice(resultSet.getDouble("PRICE"));
				muffin.setFlavor(resultSet.getString("MUFFIN_FLAVOR"));
				return muffin;
			case "BREAD":
				Bread bread = new Bread();
				bread.setId(resultSet.getInt("ID"));
				bread.setPrice(resultSet.getDouble("PRICE"));
				bread.setType(resultSet.getString("BREAD_TYPE"));
				return bread;
			case "COFFEE":
				Coffee coffee = new Coffee();
				coffee.setId(resultSet.getInt("ID"));
				coffee.setPrice(resultSet.getDouble("PRICE"));
				coffee.setRoast(resultSet.getString("COFFEE_ROAST"));
				String coffeeSize = resultSet.getString("COFFEE_SIZE");
				if(coffeeSize!=null) {
					Size size = Size.valueOf(coffeeSize);
					coffee.setSize(size);
				}
				return coffee;
			}
		}
		return null;
	}

	public Order orderFactory(ResultSet rs) throws SQLException {
		Order order = new Order();
		order.setOrderNumber(rs.getInt("ID"));
		order.setCustomerName(rs.getString("CUSTOMER"));
		order.setPhone(rs.getString("PHONE"));
		String statusString = rs.getString("ORDER_STATUS");
		boolean isValidStatus = Arrays.stream(
				OrderStatus.values())
				.map((value)->value.toString())
				.anyMatch((str)->str.equals(statusString)); // checking to see if the value is found in the enum before parsing
		if(isValidStatus) {
			order.setStatus(OrderStatus.valueOf(statusString));
		}
		String prepaidStr = rs.getString("HAS_PAID");
		boolean prepaidBool = "Y".equals(prepaidStr);
		order.setHasPrepaid(prepaidBool);
		order.setItems(new ArrayList<>());

		return order;
	}

	@Override
	public Order createNewOrder(Order order) {
		try {
			return createNewOrderHelper(order);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

}
