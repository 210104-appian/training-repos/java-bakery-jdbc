package dev.rehm;

import java.sql.SQLException;

import dev.rehm.daos.OrderDaoImpl;
import dev.rehm.models.Order;
import dev.rehm.services.OrderService;

public class OrderDriver {
	
	

	public static void main(String[] args) {
		OrderService orderService = new OrderService();
		Order result = orderService.takeOrder();
//		System.out.println(result); 
//		
		OrderDaoImpl orderDao = new OrderDaoImpl();
		orderDao.createNewOrder(result);
		for(Order o: orderDao.getAllOrders()) {
			System.out.println(o);
		}
	}
	
}
