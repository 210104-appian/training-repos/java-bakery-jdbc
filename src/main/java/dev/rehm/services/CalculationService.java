package dev.rehm.services;

import java.util.List;

import dev.rehm.models.BakeryItem;
import dev.rehm.models.Order;

public class CalculationService {
	
	public double calculateOrderSubtotal(Order o) {
		if(o==null) {
			throw new IllegalArgumentException();
		}
		List<BakeryItem> items = o.getItems();
		double subtotal = 0.0;
		if(items==null) {
			return 0.0;
		}
		for(BakeryItem item: items) {
			subtotal += item.getPrice(); //subtotal = subtotal + item.getPrice();
		}
		return subtotal;
	}

}
